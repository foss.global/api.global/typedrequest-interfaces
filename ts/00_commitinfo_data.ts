/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@api.global/typedrequest-interfaces',
  version: '3.0.1',
  description: 'interfaces for making typed requests'
}
