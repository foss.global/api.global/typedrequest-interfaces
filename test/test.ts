import { expect, tap } from '@push.rocks/tapbundle';
import * as typedrequestInterfaces from '../ts/index.js';

interface IRequestSample
  extends typedrequestInterfaces.implementsTR<
    typedrequestInterfaces.ITypedRequest,
    IRequestSample
  > {
  method: 'hey';
  request: {};
  response: {};
}

tap.test('first test', async () => {
  typedrequestInterfaces;
});

tap.start();
